﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while(true) {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double anzahlTickets;
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
//       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = fahrkartenbetellungerfassung();

       // Geldeinwurf
       // -----------
    /*   eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f\n",  ((zuZahlenderBetrag * anzahlTickets) - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag +=  eingeworfeneMünze;
       }
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
*/
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
    /*   rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag * anzahlTickets);
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    	*/}
    
    

    public static double fahrkartenbetellungerfassung()
    {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int tickets;
    	boolean ticketInputCheck = true;
    	int ticketwahl = 0;
    	
        //auszuwählende Tickets die gekauft werden möchten
    
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus \n");
    	System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
    	System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" );
    	System.out.println(	"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n");
    		
    	do { //eingabe + überprüfung ob valid
    		ticketwahl = tastatur.nextInt();
    		
    		System.out.println("Sie wählten: " + ticketwahl);
    		
    		//checks validity
    		if((ticketwahl>0) && (ticketwahl<4)) 
    			ticketInputCheck = false;
    		else
    			System.out.println(" >>falsche Eingabe<<");
    			
    	}
    	while(ticketInputCheck);
    } 
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
			System.out.printf("%s%.2f EURO %s", "Noch zu zahlen sind: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
			System.out.print("Einwurf (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
		while(true);
    			
    	}
    
    
    
    public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden gedruckt.");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
			
		public static void rueckgeldAusgeben(double rückgabebetrag) {
			if (rückgabebetrag > 0.0) {
				System.out.println("Der Rückgabewert beträgt " + rückgabebetrag + " EURO");
				System.out.println("wird in Münzen ausgezahlt:");

				while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
				{System.out.println("2 EURO");
					rückgabebetrag -= 2.0;}
				while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
				{System.out.println("1 EURO");
					rückgabebetrag -= 1.0;}
				while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
				{System.out.println("50 CENT");
					rückgabebetrag -= 0.5;}
				while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
				{System.out.println("20 CENT");
					rückgabebetrag -= 0.2;}
				while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
				{System.out.println("10 CENT");
					rückgabebetrag -= 0.1;}
				while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
				{System.out.println("5 CENT");
					rückgabebetrag -= 0.05;}
				while(true);
}
}