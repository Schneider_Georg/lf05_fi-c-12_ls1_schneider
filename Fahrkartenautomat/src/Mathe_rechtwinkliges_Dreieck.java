import java.util.Scanner;

public class Mathe_rechtwinkliges_Dreieck {
	
	


	 public static void main(String[] args)
	
	{
		Scanner tastatur = new Scanner(System.in);
		 
		double längekathete;
		double längeankathete;
		double längehypotenuse;
		double Quadratsumme;
		
		tastatur = new Scanner(System.in);
		
		System.out.printf("Länge der Kathete (cm): " );
		längekathete = tastatur.nextDouble();
	 
	
		System.out.printf("Länge der Ankathete (cm): " );
		längeankathete = tastatur.nextDouble();
		
		Quadratsumme = ((Math.pow(längekathete, 2) + Math.pow(längeankathete, 2)));
		längehypotenuse = Math.sqrt(Quadratsumme);
		System.out.printf("Die Länge der Hypotenuse beträgt: %.2f cm", längehypotenuse);
		
	}

}
