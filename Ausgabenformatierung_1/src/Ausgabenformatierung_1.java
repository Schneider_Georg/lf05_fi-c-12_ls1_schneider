
public class Ausgabenformatierung_1 {

	public static void main(String[] args) 
	{
		// Erste Aufgabe.
		/*
		 * Damit die beiden S�tze nebeneinander erscheinen muss beim Print das "LN" entfernt werden damit in keiner neuen Zeile Ausgegeben wird.
		 * Ein Lerrzeichen nach dem ersten Satz innerhalb der Anf�hrungsstriche sollte gesetzt werden damit die S�tze vonenander getrennt sind
		 * PrinLN bedeutet ausgabe in einer neuen Zeile in der Console. Nur Print bedeutet keine neue Zeile. 
		 * 
		 * \n sorgt f�r eine Zeilenumbruch innerhalb der Systemout-Ausgabe
		 */
		System.out.print("Das ist eine Ausgabe! \n" + "So siehe diese Ausgabe!\n"); // ohne println aber mit \n
		
		System.out.println("Das ist eine Ausgabe! ");
		System.out.println("Das ist eine Ausgabe! ");// mit println aber ohne \n

		/*
 * 
 */
	}

}
