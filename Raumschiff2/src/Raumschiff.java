import java.util.ArrayList;
import java.util.Random;
/**
 * @author g.schneider
 * @since 12.03.22
 * @version 3.5
 */
public class Raumschiff {
/**
 * {@code}@ Setzung der Einsehbarkeit
 */
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Konstruktor()
	 */
	public Raumschiff() {
		
	}
/**
 * 
 * @param photonentorpedoAnzahl
 * @param energieversorgungInProzent
 * @param schildeInProzent
 * @param huelleInProzent
 * @param lebenserhaltungssystemeInProzent
 * @param schiffsname
 * @param androidenAnzahl
 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
/**
 * 
 * @return
 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
/**
 * 
 * @param photonentorpedoAnzahl
 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
/**
 * 
 * @return
 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
/**
 * 
 * @param energieversorgungInProzent
 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
/**
 * 
 * @return
 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
/**
 * 
 * @param schildeInProzent
 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
/**
 * 
 * @return
 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
/**
 * 
 * @param huelleInProzent
 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
/**
 * 
 * @return
 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
/**
 * 
 * @param lebenserhaltungssystemeInProzent
 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
/**
 * 
 * @return
 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
/**
 * 
 * @param androidenAnzahl
 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
/**
 * 
 * @return
 */
	public String getSchiffsname() {
		return schiffsname;
	}
/**
 * 
 * @param schiffsname
 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
/**
 * 
 * @param neueLadung
 */
	public void addLadung(Ladung neueLadung) {

		this.ladungsverzeichnis.add(neueLadung);
	}
/**
 * 
 * @param r
 */
	
	public void phontonentorpedoSchiessen(Raumschiff r) {
		System.out.println(schiffsname);
		System.out.println("================");
		if (photonentorpedoAnzahl == 0) {
			System.out.println("Keine Phontonentorpedos gefunden!\n");
			r.nachrichtAnAlle("-=*Click*=-\n");
		} else {
			r.nachrichtAnAlle("Phontonentorpedo abgeschossen\n");
			photonentorpedoAnzahl -= 1;
			r.treffer(r);
		}
	}
/**
 * 
 * @param r
 */
	public void phontonentorpedosLaden(Raumschiff r) {

	}
/**
 * 
 * @param r
 */
	public void phaserkanoneSchiessen(Raumschiff r) {

	}
/**
 * 
 * @param r
 */
	private void treffer(Raumschiff r) {

	}
/**
 * 
 * @param message
 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}

/**
 * 
 * @param schutzschilde
 * @param energieversorgung
 * @param schiffshuelle
 * @param anzahlDroiden
 */

	public void reparaturDurchfuehren(boolean schutzschilde, 
									  boolean energieversorgung, 
									  boolean schiffshuelle,
									  int anzahlDroiden) {	
		
	}
/**
 * @param Ausgabe Zustand Raumschiff
 */
	public void zustandRaumschiff() {
		System.out.println("Zustand des Raumschiffs");
		System.out.println("=======================");
		System.out.println("Raumschiff: " + schiffsname);
		System.out.println("Photonentorpedo Anzahl: " + photonentorpedoAnzahl);
		System.out.println("Energieversorgung in Prozent: " + energieversorgungInProzent);
		System.out.println("Schilde in Prozent: " + schildeInProzent);
		System.out.println("Huelle in Prozent: " + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + lebenserhaltungssystemeInProzent);
		System.out.println("Androiden Anzahl: " + androidenAnzahl + "\n");
	}

	public void ladungsverzeichnisAusgeben() {
		
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung.getBezeichnung() + ": " + ladung.getMenge());
		}
	}
public void ladungsverzeichnisAufraeumen() {

		int save;
		for (int j = 0; j < ladungsverzeichnis.size(); j++) {
			save = ladungsverzeichnis.get(j).getMenge();
			if (save == 0) {
				ladungsverzeichnis.remove(j);
			}
		}
		
	}
/**
 * R�ckgabe des Strings
 */
	public String toString() {
		return "Raumschiff" + ", "
				+ ", energieversorgungInProzent = " + energieversorgungInProzent +
				", SchildInProzent = " + schildeInProzent + 
				", h�lleInProzent = " + huelleInProzent + 
				", LebenserhaltungssystemInProzent = " + lebenserhaltungssystemeInProzent + 
				", AndroidenAnzahl = " + androidenAnzahl + 
				", Schiffname = " + schiffsname + 
				", Broadcastkommunikator = " + broadcastKommunikator + 
				", Ladungsverzeichnis" + ladungsverzeichnis;
	}
}
