public class RaumschiffTest {

	public static void main(String[] args) {
				
		/**
		 * @author g.schneider
		 * @since 18.05.22
		 * @version 3.5
		 */
		Raumschiff p1 = new Raumschiff ();
		Raumschiff p2 = new Raumschiff();
		Raumschiff p3 = new Raumschiff();
		
		
		Ladung l1 = new Ladung("todesmaus", 200 );
		Ladung l2 = new Ladung("blitzer", 200);
		Ladung l3 = new Ladung("Darkschrott", 5);
		Ladung l4 = new Ladung("Knete", 2 );
		Ladung l5 = new Ladung("Plasmapups", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		/**
		 * Setzungen der Atributte der Schiffe
		 */
		p1.setPhotonentorpedoAnzahl(1);
		p1.setEnergieversorgungInProzent(100);
		p1.setSchildeInProzent(100);
		p1.setAndroidenAnzahl(2);
		p1.setHuelleInProzent(100);
		p1.setLebenserhaltungssystemeInProzent(100);
		p1.setSchiffsname("Reiner");
		p1.addLadung(l1);
		p1.addLadung(l2);
	
		p2.setPhotonentorpedoAnzahl(2);
		p2.setEnergieversorgungInProzent(100);
		p2.setSchildeInProzent(100);
		p2.setAndroidenAnzahl(2);
		p2.setHuelleInProzent(100);
		p2.setLebenserhaltungssystemeInProzent(100);
		p2.setSchiffsname("Lokomo");
		p2.addLadung(l7);
		p2.addLadung(l6);
		p2.addLadung(l5);

		p3.setPhotonentorpedoAnzahl(0);
		p3.setEnergieversorgungInProzent(80);
		p3.setSchildeInProzent(80);
		p3.setAndroidenAnzahl(5);
		p3.setHuelleInProzent(50);
		p3.setLebenserhaltungssystemeInProzent(100);
		p3.setSchiffsname("Heinz");
		p3.addLadung(l3);
		p3.addLadung(l4);
		
		/**
		 * Ausgabe der jeweiligen Schiffe
		 * 
		 */
		System.out.println("Schiffzust�nde");
		System.out.println("-------------");
		System.out.println(p1.getSchiffsname());
		p1.zustandRaumschiff();
		System.out.println("_____________");
		System.out.println(p2.getSchiffsname());
		p2.zustandRaumschiff();
		System.out.println("_____________");
		System.out.println(p3.getSchiffsname());
		p3.zustandRaumschiff();
		System.out.println("_____________");
		
		System.out.println("Ladungsverzeichnis der Schiffe");
		System.out.println("-------------");
		System.out.println(p1.getSchiffsname());
		p1.ladungsverzeichnisAusgeben();
		System.out.println("_____________");
		System.out.println(p2.getSchiffsname());
		p2.ladungsverzeichnisAusgeben();
		System.out.println("_____________");
		System.out.println(p3.getSchiffsname());
		p3.ladungsverzeichnisAusgeben();
		System.out.println("_____________");
		
		p3.phontonentorpedosLaden(p3);
		p3.phontonentorpedoSchiessen(p2);
		p3.ladungsverzeichnisAusgeben();
		p3.ladungsverzeichnisAufraeumen();
		p3.zustandRaumschiff();
		
	}
}
