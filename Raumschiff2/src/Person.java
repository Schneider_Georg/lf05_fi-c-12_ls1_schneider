/**
 * Person - Klasse
 * Teil von Schulverwaltungspramm
 * 
 * 
 * @author g.schneider
 * @since 18.05.21
 * @version 2.0
 * 
 */
public class Person {

	
	
			private String vorname;
			private String nachname;
			private int alter;
			
			public Person( ) {
				
				
				
			}
			
public Person(String vorname, String nachname, int alter) {
				
				this.vorname = vorname;
				this.nachname = nachname;
				this.alter = alter;
			}

/**
 * 
 * @return String
 */
			public String getVorname() {
				return vorname;
			}

			public void setVorname(String vorname) {
				this.vorname = vorname;
			}
/**
 * 
 * @return String
 */
			public String getNachname() {
				return nachname;
			}

			public void setNachname(String nachname) {
				this.nachname = nachname;
			}
/**
 * 
 * @return int
 */
			public int getAlter() {
				return alter;
			}

			public void setAlter(int alter) {
				this.alter = alter;
			}
			/**
			 * 
			 * @param menge
			 * @param art
			 */
			public void esse(int menge, String art) {
				
			}
			
			public String toString () {
				return  "Vorname:   " + vorname + "\n" + 
						"Nachname:  " + nachname + "\n" + 
						"Alter:     " + alter + "\n";
			}
}
